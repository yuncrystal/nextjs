/**
 * Created by YUN on 2019-03-10.
 */

import variables from '../util/variables';


const selectDrawerMenu = (selectedID) => {
    return {
        type: variables.MENU.CHANGE_SELECTED_MENU,
        payload: selectedID
    }
}

export {
    selectDrawerMenu
}