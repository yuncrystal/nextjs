/**
 * Created by YUN on 7/3/19.
 */
import React,{useState} from 'react';
import Nav from './components/nav';
import SearchBar from './components/SearchBar';
import ImageList from './components/ImageList';
import unsplash from './api/unsplash';


const  CarList = ()=>{

    const [images,setImages] = useState([]);
    
    const searchSubmit = async (term) => {
        const response = await unsplash.get('/search/photos',{
            params:{ query : term }
        })

        setImages(response.data.results)

    }
    return (
        <Nav>
            <SearchBar onSearchSubmit={searchSubmit}/>
               
            <ImageList resources={images}/>
        </Nav>
    )
    
}

export default CarList;