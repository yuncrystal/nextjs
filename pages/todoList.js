
import React from "react";
import Link from 'next/link'
import Nav from './components/nav';


const TodoItem = (props) => <li onClick={props.onClick}>{props.item.text}</li>

const items = [ { text: 'Buy grocery', done: true },
    { text: 'Play guitar', done: false },
    { text: 'Romantic dinner', done: false }
];

class TodoList extends React.Component {

    handleItemClick(item) {
        // console.log('item:',this.item);
    }

    render() {
        return (
            <Nav>
                <Link href='/' as='/'>
                    <a className="about-link" style={{color:'#ff0000'}}>Go about page</a>
                </Link>
                <ul>
                    {items.map((item,index) => <TodoItem key={index} item={item} onClick={this.handleItemClick(item)}/>)}
                </ul>

            </Nav>
           );
    }


}

export default TodoList