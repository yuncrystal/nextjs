import React,{useState} from 'react';
import PropTypes from 'prop-types';


 const SearchBar = (props)=> {

    const [term,setTerm] = useState('');
   
    const onFormSubmit = (event)=>{
        event.preventDefault();
        props.onSearchSubmit(term);
        
    }
    return (
        <div className='ui segment'>
            <form onSubmit={onFormSubmit} className='ui form'>
                <div className='field'>
                    <label>Image Search</label>
                    <input 
                        type="text"
                        value={term} 
                        onChange={(e)=>setTerm(e.target.value)}/>

                </div>
            </form>

        </div>
    )
}

SearchBar.propTypes = {
    onSearchSubmit:PropTypes.func.isRequired,
}

export default SearchBar;