/**
 * Created by YUN on 2019-03-07.
 */
import React from "react";
import Link from 'next/link';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import lightBlue from '@material-ui/core/colors/lightBlue';
import {selectDrawerMenu} from '../../action/menuAction';




const drawerWidth = 240;

const drawerMenu = [
    {
        href:'/todoList',
        text:'To Do List',
    },
    {
        href:'/tvShows',
        text:'Marvel TV Shows',
    },
    {
        href:'/productList',
        text:'Product List',
    },
    {
        href:'/carList',
        text:'Car List',
    },
]

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        backgroundColor:lightBlue[500],
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
});


class Nav extends React.Component {

    state = {
        mobileOpen:false,
    }

    handleDrawerToggle = ()=>{
        this.setState( state => ({
            mobileOpen:!state.mobileOpen
        }))
    }


    render() {

        const { classes,theme,menu,selectDrawerMenu } = this.props;

        const drawer = (
            <div className={classes.toolbar}>
                <div className={classes.toolbar} />
                <Divider/>
                <List>
                    {
                        drawerMenu.map((drawer,index)=>{
                            return (
                                <Link href={drawer.href} passHref key={index}>
                                    <ListItem  button onClick={()=>selectDrawerMenu(index)} selected={menu.selectedMenuId === index }>
                                        <ListItemIcon>
                                            <InboxIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={drawer.text} />
                                    </ListItem>
                                </Link>
                            )
                        })
                    }
                </List>
            </div>
        )

        return (
            <div className={classes.root}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" noWrap>
                           YUN
                        </Typography>
                    </Toolbar>

                </AppBar>
                <nav className={classes.drawer}>
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    {
                        this.props.children
                    }
                </main>



                {/*<Link href={'/todoList'} as='/Marvel TV Shows'>*/}
                {/*<a className="about-link" style={{color:'#ff0000'}}>Marvel TV Shows</a>*/}
                {/*</Link>*/}
            </div>
        )
    }
}



Nav.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps (state) {
    const { menu } = state
    return {
        menu
    }
}


export default connect(mapStateToProps,{selectDrawerMenu})(withStyles(styles, { withTheme: true })(Nav));