import React from 'react';
import '../../styles/ImageList.scss';

const ImageList = (props) =>{
    const images = props.resources.map(({urls})=>{
        return (
            <img src={urls.regular} alt={urls.description}/>
        )
    })

    return (
        <div className='image-list'>
            {images}
        </div>
    )
}

export default ImageList;