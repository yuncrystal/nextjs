/**
 * Created by YUN on 12/11/18.
 */

import React from "react";

export default class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            name:'',
        };
        this.fileInput = React.createRef();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(value,event) {
        console.log(event)
        this.setState({[value]: event.target.value});
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        console.log('ref:',this.fileInput.current)
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                    <input type="text" value={this.state.value} onChange={(e)=>this.handleChange('value',e)} />
                </label>
                <input type="text" value={this.state.name} onChange={(e)=>this.handleChange('name',e)}/>
                <br/>

                <input type="file" ref={this.fileInput}/>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}