/**
 * Created by YUN on 5/3/19.
 */
import React from 'react';
import Link from 'next/link'
import Nav from './components/nav';


const ProductContainer = (props) =>{
    return (
        <div style={{width:100,height:100,borderWidth:1,borderColor:'black',borderStyle:'solid'}}>

            <span>{props.title}</span>
            <Link href={`/productDetail?title=${props.en_title}`} as={`product/${props.en_title}`}>
                <a className="about-link" style={{color:'#ff0000'}}>{props.en_title}</a>
            </Link>

        </div>
    )

}


class ProductList extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <Nav>
                <Link href='/' as='/'>
                    <a className="about-link" style={{color:'#ff0000'}}>Product List Page</a>
                </Link>
                <ProductContainer title={'牛奶'} en_title={'milk'}/>
                <ProductContainer title={'鸡蛋'}  en_title={'egg'}/>
                <ProductContainer title={'面包'}  en_title={'bread'}/>
                <ProductContainer title={'茄子'}  en_title={'eggplant'}/>
                <ProductContainer title={'西红柿'} en_title={'tomato'}/>
            </Nav>
        )
    }
}

// function mapStateToProps(state) {
//     return {
//         userInfo: state.userInfo
//     }
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         postActions: bindActionCreators(postActions,dispatch)
//     }
// }


export default ProductList;