/**
 * Created by YUN on 5/3/19.
 */
import React from 'react';
import { withRouter } from 'next/router';

class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        const {router} = this.props;
        return (
            <div>
                <h1>{`${router.query.title} Detail Page`}</h1>
            </div>
        )
    }
}


// function mapDispatchToProps(dispatch) {
//     return {
//         postActions: bindActionCreators(postActions,dispatch)
//     }
// }


export default withRouter(ProductDetail);