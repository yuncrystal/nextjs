
import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 4ea3dc242ae34eec75ba1ba206149a41ab0910feaf884bb884b000614e6f2968'
    }
});


