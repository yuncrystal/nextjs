/**
 * Created by YUN on 2019-03-10.
 */

import variables from '../util/variables';

const INITIAL_STATE = {
    selectedMenuId: 0,
}
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case variables.MENU.CHANGE_SELECTED_MENU:

            return {...state,selectedMenuId:action.payload}

        default:
            return state;
    }
}