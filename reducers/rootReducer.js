/**
 * Created by YUN on 2/6/17.
 */
import {combineReducers} from 'redux';
import MenuReducer from './menuReducer';

const rootReducer = combineReducers({
    menu:MenuReducer,

});

export default rootReducer;
