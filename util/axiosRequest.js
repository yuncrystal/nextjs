/**
 * Created by YUN on 3/10/18.
 */

import axios from 'axios';

axios.defaults.baseURL = 'https://api.tvmaze.com/';
axios.defaults.retryDelay = 5000;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';


const getRequest = (url)=>{
    return axios({
        method:'get',
        url:url,
    });
}

const postRequest = (url,obj)=>{
    return axios({
        method:'post',
        url:url,
        data:obj,
    });
}




const putRequest = (url,obj)=>{
    return axios({
        method:'put',
        url:url,
        data:obj,
    });

}

const patchRequest = (url,obj)=>{
    return axios({
        method:'patch',
        url:url,
        data:obj,
    });

}

const deleteRequest =(url,obj)=>{
    return axios({
        method:'delete',
        url:url,
        data:obj,
    });
}


export {
    getRequest,
    postRequest,
    putRequest,
    patchRequest,
    deleteRequest

}
