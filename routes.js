/**
 * Created by YUN on 2019-03-11.
 */

const express = require('express');
const router = express.Router();

const user_controller = require('./controllers/userController');

// Home page route.
router.get('/wiki', function (req, res) {
    res.send('Wiki home page');
})

// About page route.
router.get('/us', function (req, res) {
    res.send('About this wiki');
});

router.get('/user',user_controller.user_list);

module.exports = router;