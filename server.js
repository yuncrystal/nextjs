/**
 * Created by YUN on 7/3/19.
 */

const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const router = require('./routes');

app.prepare()
    .then(() => {
        const server = express();


        server.get('/product/:en_title', (req, res) => {
            const actualPage = '/productDetail';
            const queryParams = { title: req.params.en_title };
            app.render(req, res, actualPage, queryParams);
        });



        server.use('/',router);

        server.get('*', (req, res) => {
            return handle(req, res);
        });

        server.listen(3000, (err) => {
            if (err) throw err;
            console.log('> Ready on http://localhost:3000');
        });
    })
    .catch((ex) => {
        console.error(ex.stack);
        process.exit(1);
    });